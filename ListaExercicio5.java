import java.util.Locale;
import java.util.Scanner;

public class ListaExercicio5 {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite o primeiro valor: ");
		int valor1 = out.nextInt();
		System.out.println("Digite o segundo valor: ");
		int valor2 = out.nextInt();
		
		if (valor1 > valor2) {
			System.out.printf("O primeiro valor %d é maior", valor1);
		} else if (valor1 < valor2) {
			System.out.printf("O segundo valor %d é maior", valor2);
		} else {
			System.out.println("Os dois valores são iguais");
		}
		
		out.close();

	}

}
