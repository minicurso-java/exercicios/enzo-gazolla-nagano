import java.util.ArrayList;
import java.util.Scanner;

public class ListaExercicio17 {
	
	public static boolean ePrimo(int numero) {
	    int raiz = (int) Math.sqrt(numero);
	    for (int i = 2; i <= raiz; i++) {
	        if (numero % i == 0) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite um valor inicial: ");
		int valor = out.nextInt();
		
		System.out.println("Digite um valor final: ");
		int valor2 = out.nextInt();
		
		ArrayList<Integer> numerosPrimos = new ArrayList<Integer>();
		
		for (int i = valor; i <= valor2; i++) {
			if (ePrimo(i)) {
				numerosPrimos.add(i);
			}
		}
		System.out.println(numerosPrimos);
		
		out.close();
	}

}
