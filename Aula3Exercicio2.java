import java.util.Scanner;

public class Aula3Exercicio2 {
	
	public static void verificarMultiploDeDois (int num) {
		if (num % 2 == 0) {
			System.out.println("O número " + num + " é múltiplo de 2");
		} 
		else {
			System.out.println("O número " + num + " não é múltiplo de 2");
		}
		
	}

	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		int numero;
		
		System.out.println("Digite um número: ");
		numero = out.nextInt();
		
		verificarMultiploDeDois (numero);
		
		out.close();
	}

}