import java.util.Locale;
import java.util.Scanner;

public class Aula1Exercicio2 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		
		int A,B,C,D;
		Scanner out = new Scanner(System.in);
		System.out.println("Digite o valor de A: ");
		A = out.nextInt();
		System.out.println("Digite o valor de B: ");
		B = out.nextInt();
		System.out.println("Digite o valor de C: ");
		C = out.nextInt();
		System.out.println("Digite o valor de D: ");
		D = out.nextInt();
		
		
		//Cálculo
		int diferenca = (A * B - C * D);
		
		System.out.printf("Diferença: %d \n", diferenca);

	}

}
