import java.util.Scanner;

public class Aula6Exercicio4 {

	public static void main(String[] args) {

		Scanner out = new Scanner (System.in);

		
		System.out.println("Digite o tamanho do Array: ");
		int tamanhoDoArray = out.nextInt();
		

		int array [] = new int[tamanhoDoArray];

		int maiorValor= 0;
		int posicaoMaior = 0;
		
		for (int i = 0; i < tamanhoDoArray; i++) {
			
			System.out.println("Digite o valor do Array: ");
			int valorArray = out.nextInt();
			array[i] = valorArray;
			
			if (array[i] > maiorValor) {
				maiorValor = array[i];
				posicaoMaior = i;
			}
			
		}
		
		System.out.printf("O maior valor é %d \n", maiorValor);
		System.out.printf("A posição maior do valor é %d", posicaoMaior);

		out.close();
	}

}
