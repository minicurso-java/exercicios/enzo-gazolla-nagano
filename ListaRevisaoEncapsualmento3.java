
public class ListaRevisaoEncapsualmento3 {

	public static void main(String[] args) {

		ContaCorrente contaCorrente = new ContaCorrente();
		contaCorrente.setTitular("Enzo");
		System.out.printf("Titular: %s \n", contaCorrente.getTitular());
		contaCorrente.setSaldo(200);
		System.out.println(contaCorrente);
		contaCorrente.deposito(300);
		System.out.println(contaCorrente);
		contaCorrente.saque(100);
		System.out.println(contaCorrente);

	}

}
class ContaCorrente {

	private double saldo;
	private String titular;
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	@Override
	public String toString() {
		return "ContaCorrente [saldo = " + saldo + ", titular = " + titular + "]";
	}
	public void deposito(double valor) {
		System.out.printf("Você depositou R$ %.2f \n", valor);
		saldo += valor;
	}
	public void saque (double valor) {
		if (saldo < valor) {
			System.out.println("Você não tem dinheiro para sacar esse valor \n");
			return;
		}
		System.out.printf("Você sacou R$ %.2f \n", valor);
		saldo -= valor;
	}
}