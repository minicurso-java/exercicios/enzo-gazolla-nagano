import java.util.Locale;
import java.util.Scanner;

public class ListaExercicio4 {

	public static int Fatorial(int valor) {
		if (valor == 0) {
			return 1;
		}
		return valor * Fatorial(valor - 1);
	}
	
	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite um valor: ");
		int valor = out.nextInt();
		
		
		int total = Fatorial(valor);
		
		System.out.printf("Fatorial de %d = %d", valor, total);
		
		out.close();
	}

}
