import java.util.Scanner;

public class ListaExercicio20 {

	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);

		System.out.println("Digite um ano: ");
		int ano = out.nextInt();


		if ((ano % 4 == 0 && ano % 100 != 0) || ano % 400 == 0) {
			System.out.printf("%d é um ano bissexto", ano);
		} else {
			System.out.printf(" %d não é um ano bissexto", ano);
		}
		out.close();
	}
}


