import java.util.Scanner;

public class Aula3Exercicio1 {
	
	public static int multiplicar(int a, int b) {
		return a * b;
	}

	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		int num1, num2;
		int resultado;
		
		System.out.println("Digite o primeiro número: ");
		num1 = out.nextInt();
		System.out.println("Digite o segundo número: ");
		num2 = out.nextInt();
		
		resultado = multiplicar(num1, num2);
		
		System.out.println("O resultado da multiplicação e: " + resultado);
        
		out.close();

	}

}