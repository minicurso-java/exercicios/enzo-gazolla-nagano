
public class ListaRevisaoEncapsulamento2 {

	public static void main(String[] args) {

		CircuitoEletronico circuitoEletronico = new CircuitoEletronico();
		circuitoEletronico.setCorrente(20);
		circuitoEletronico.setTensao(220);
		System.out.println(circuitoEletronico);
		System.out.printf("Resistencia: %.2f ohms\n", circuitoEletronico.calcularResistencia());
		System.out.printf("Potencia dissipada: %.2f watts", circuitoEletronico.calcularPotencia());
	}

}
class CircuitoEletronico {

	private double tensao;
	private double corrente;
	public double getTensao() {
		return tensao;
	}
	public void setTensao(double tensao) {
		this.tensao = tensao;
	}
	public double getCorrente() {
		return corrente;
	}
	public void setCorrente(double corrente) {
		this.corrente = corrente;
	}
	@Override
	public String toString() {
		return "CircuitoEletronico [tensao = " + tensao + ", corrente = " + corrente + "]";
	}
	public double calcularResistencia() {
		if (corrente == 0) {
			System.out.println("A corrente não pode ser zero");
		}
		return tensao / corrente;
	}
	public double calcularPotencia() {
		return corrente * corrente * calcularResistencia();
	}
}