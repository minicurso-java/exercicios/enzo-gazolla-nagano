import java.util.Scanner;

public class ListaExercicio18 {

	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite uma palavra: ");
		String palavra = out.nextLine();
		
		System.out.println("Digite um caractere: ");
		char caractere = out.next().charAt(0);
		
		char [] charPalavra = palavra.toLowerCase().toCharArray();
		
		int total = 0;
		
		for (int i = 0; i < charPalavra.length; i++) {
			if (charPalavra[i] == caractere) {
				total++;
			}
		}
		System.out.printf("O caractere %c aparece %d vezes na palavra %s", caractere, total, palavra);
		out.close();
	}

}
