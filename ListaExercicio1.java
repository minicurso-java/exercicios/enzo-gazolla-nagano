import java.util.Locale;
import java.util.Scanner;

public class ListaExercicio1 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite o primeiro valor:");
		int valor1 = out.nextInt();
		System.out.println("Digite o segundo valor:");
		int valor2 = out.nextInt();
		
		int soma = valor1 + valor2;
		
		System.out.printf("A soma dos valores é %d", soma );
		out.close();
	}

}
