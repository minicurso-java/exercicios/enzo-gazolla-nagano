
public class Aula8FigurasGeométricas {

	public static void main(String[] args) {

		var quadrado = new Quadrado ();
		quadrado.lado = 10;
		System.out.println("Quadrado: ");
		quadrado.calcularArea();
		quadrado.calcularPerimetro();

		var circulo = new Circulo ();
		circulo.raio = 8;
		System.out.println("Circulo: ");
		circulo.calcularArea();
		circulo.calcularPerimetro();

		var triangulo = new Triangulo ();
		triangulo.lado1 = 5;
		triangulo.lado2 = 6;
		triangulo.lado3 = 7;
		System.out.println("Triangulo: ");
		triangulo.calcularArea();
		triangulo.calcularPerimetro();

	}

}
abstract class FiguraGeometrica {

	double area;
	double perimetro;
	abstract void calcularArea ();
	abstract void calcularPerimetro ();

}

class Quadrado extends FiguraGeometrica {

	double lado;

	@Override
	void calcularArea() {
		area = lado * lado;
		System.out.printf("Area do quadrado: %.2f \n", area);
	}
	@Override
	void calcularPerimetro() {
		perimetro = 4 * lado;
		System.out.printf("Perimetro do quadrado: %.2f \n\n", perimetro);
	}

}

class Circulo extends FiguraGeometrica {

	double raio;

	@Override
	void calcularArea() {
		area = Math.PI * raio * raio;
		System.out.printf("Area do circulo: %.2f \n", area);
	}
	@Override
	void calcularPerimetro() {
		perimetro = 2 * Math.PI * raio;
		System.out.printf("Perimetro do circulo: %.2f \n\n", perimetro);
	}

}

class Triangulo extends FiguraGeometrica {

	double lado1;
	double lado2;
	double lado3;

	@Override
	void calcularArea() {
		double s = (lado1 + lado2 + lado3)/2;
		area =  Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3));
		System.out.printf("Area do Triangulo: %.2f \n", area);
	}
	@Override
	void calcularPerimetro() {
		perimetro = lado1 + lado2 + lado3;
		System.out.printf("Perimetro do Triangulo: %.2f \n\n", perimetro);
	}

}