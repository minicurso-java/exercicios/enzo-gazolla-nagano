import java.util.Scanner;

public class ListaExercicio13 {

	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		double principal = 0;
		double taxa = 0;
		double tempo = 0;
		
		System.out.println("Qual o valor principal: ");
		principal = out.nextDouble();
		System.out.println("Qual a taxa: ");
		taxa = out.nextDouble();
		System.out.println("Qual o tempo: ");
		tempo = out.nextDouble();
		
		double total = principal * taxa * tempo;
		
		System.out.printf("O valor do juros simples é %.2f ", total);
		
		out.close();
	}

}
