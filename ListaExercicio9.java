import java.util.Scanner;

public class ListaExercicio9 {

	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		int[] array = new int[3];
		int maiorValor = 0;
		
		System.out.println("Insira o primerio valor: ");
		array[0] = out.nextInt();
		System.out.println("Insira o segundo valor: ");
		array[1] = out.nextInt();
		System.out.println("Insira o terceiro valor: ");
		array[2] = out.nextInt();
		
		for(int i = 0; i < array.length; i++) {
			if (array[i] > maiorValor) {
				maiorValor = array[i];
			}
		}
		System.out.printf("O maior valor é %d", maiorValor);
		out.close();
	}

}
