import java.util.Locale;
import java.util.Scanner;

public class Aula1Exercicio1 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		
		double A,B,C;
		Scanner out = new Scanner(System.in);
		System.out.println("Digite o valor de A: ");
		A = out.nextDouble();
		System.out.println("Digite o valor de B: ");
		B = out.nextDouble();
		System.out.println("Digite o valor de C: ");
		C = out.nextDouble();
		
		// Cálculo 
		double areaDoTriangulo = (A * C) / 2;
		double areaDoCirculo =  3.14159 * C * C;
		double areaDoTrapezio = ((A + B) * C) / 2;
		double areaDoQuadrado = B * B;
		double areaDoRetangulo = A * B;
		
		System.out.printf("Triangulo: %.2f \n", areaDoTriangulo);
		System.out.printf("Circulo: %.2f \n", areaDoCirculo);
		System.out.printf("Trapezio: %.2f \n", areaDoTrapezio);
		System.out.printf("Quadrado: %.2f \n", areaDoQuadrado);
		System.out.printf("Retangulo: %.2f \n", areaDoRetangulo);
		
		out.close();
		
		
		
		

	}

}
