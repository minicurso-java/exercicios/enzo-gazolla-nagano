import java.util.Locale;
import java.util.Scanner;

public class ListaExercicio8 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite a temperatura em Celsisus: ");
		double celsius = out.nextDouble();
		
		double fahrenheit = (celsius * 9/5) + 32;
		
		System.out.printf("A temperatura em Fahrenheit é: %.2f", fahrenheit);
		
		out.close();

	}

}
