import java.util.Scanner;

public class ListaExercicio10 {

	
	public static void main(String[] args) throws InterruptedException {
		
		Scanner out = new Scanner(System.in);
		
		int num = 0;
		
		System.out.println("Digite um numero: ");
		num = out.nextInt();
		
		for (int i = 0; i <= 10; i++) {
			System.out.printf("%d * %d = %d \n", num, i ,  num * i);
			Thread.sleep(1000);
				
		}
		
		out.close();
	}

}
