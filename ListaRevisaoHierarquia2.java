
public class ListaRevisaoHierarquia2 {

	public static void main(String[] args) {

		Livro livro = new Livro("Rede de Computadores", 29.99, "CAMPUS");
		livro.resumo();

		Eletronico eletronico = new Eletronico("Iphone", 599.99);
		eletronico.celular();

		Roupa roupa = new Roupa("Henley", 49.99);
		roupa.tamanho();
		roupa.marca();

	}

}

class Produto {

	String nome;
	double preco;
	Produto(String nome, double preco) {
		this.nome = nome;
		this.preco = preco;
		System.out.printf("Nome: %s R$%.2f \n", nome, preco);
	}

}

class Livro extends Produto {

	String editora;
	public Livro(String nome, double preco, String editora) {
		super(nome, preco);
		this.editora = editora;
		System.out.printf("Editora: %s \n", editora);

	}
	void resumo () {
		System.out.println("Esse Livro fala sobre Rede de Computadores \n");
	}
}

class Eletronico extends Produto {

	public Eletronico(String nome, double preco) {
		super(nome, preco);
	}
	void celular () {
		System.out.println("Esse é um celular da apple \n");
	}
}

class Roupa extends Produto {

	public Roupa(String nome, double preco) {
		super(nome, preco);
	}
	void tamanho () {
		System.out.println("Esse é do tamanho M ");
	}
	void marca () {
		System.out.println("Essa roupa é da marca Old Navy \n");
	}
}
