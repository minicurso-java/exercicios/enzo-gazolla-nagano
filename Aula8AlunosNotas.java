import java.util.Arrays;

public class Aula8AlunosNotas {
		
	public static void main(String[] args) {
		
		double [] enzoNotas = {7.8, 7.5, 8.5};
		Aluno enzo = new Aluno("Enzo", 22401710, enzoNotas);
		
		double media = enzo.calcularMedia();
		enzo.verificarAprovacao(media);
		
	}
}

class Aluno {
	
	String nome;
	int matricula;
	double[] notas;
	
	public Aluno(String nome, int matricula, double[] notas) {
		this.nome = nome;
		this.matricula = matricula;
		this.notas = notas;
		System.out.printf("Aluno: %s; Matrícula: %d, Notas: %s \n", nome, matricula, Arrays.toString(notas));
	}
	
	public double calcularMedia() {
        double soma = 0;
        for (double nota : notas) {
            soma += nota;
        }
        double media =  soma / notas.length;
        System.out.printf("A média é igual a: %.2f \n", media);
        return media;
    }

    public void verificarAprovacao(double mediaAprovacao) {
      
        
        if (mediaAprovacao >= 7.0) {
        	System.out.printf(" %s foi aprovado", nome);
        } else {
        	System.out.printf(" %s foi reprovado", nome);
        }
        
    }
    
}

