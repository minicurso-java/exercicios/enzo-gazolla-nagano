
public class ListaRevisaoHierarquia3 {

	public static void main(String[] args) {

		Mamifero mamifero = new Mamifero("Ursinho Po", 48, 2.8);
		mamifero.Som();

		Ave ave = new Ave("Galinha", 7, 0.28);
		ave.Som();

		Reptil reptil = new Reptil ("Crocodilo", 102, 4.2);
		reptil.Som();
	}

}
class Animal {

	String nome;
	int idade;
	double tamanho;
	public Animal(String nome, int idade, double tamanho) {
		this.nome = nome;
		this.idade = idade;
		this.tamanho = tamanho;
		System.out.printf("Animal: %s Idade: %d Tamanho: %.2f \n", nome, idade, tamanho);

	}

}
class Mamifero extends Animal {

	public Mamifero(String nome, int idade, double tamanho) {
		super(nome, idade, tamanho);
	} 
	void Som() {
		System.out.println("Som: Roar!! \n");
	}
}

class Ave extends Animal {

	public Ave(String nome, int idade, double tamanho) {
		super(nome, idade, tamanho);
	} 
	void Som() {
		System.out.println("Som: cocoricó! \n");
	}
}
class Reptil extends Animal {

	public Reptil(String nome, int idade, double tamanho) {
		super(nome, idade, tamanho);
	} 
	void Som() {
		System.out.println("Som: rugir! \n");
	}

}


