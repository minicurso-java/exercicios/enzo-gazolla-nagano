import java.util.Locale;
import java.util.Scanner;

public class ListaExercicio3 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite um número: ");
		int numero = out.nextInt();
		
		if (numero % 2 == 0) {
			System.out.println("O número é Par");
		}
		else {
			System.out.println("O número é Impar");
		}
		
		out.close();
	}

}
