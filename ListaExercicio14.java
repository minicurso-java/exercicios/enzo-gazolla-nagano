import java.util.Scanner;

public class ListaExercicio14 {

	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite uma palavra: ");
		String palavra = out.nextLine();
		
		char [] charPalavra = palavra.toLowerCase().toCharArray();
		int total = 0;
		
		for (int i = 0; i < charPalavra.length; i++) {
			if (charPalavra[i] == 'a' || charPalavra[i] == 'e' || charPalavra[i] == 'i' || charPalavra[i] == 'o' || charPalavra[i] == 'u') {
			total++;
			}
		}
		System.out.printf("A palavra tem %d vogais", total);
		out.close();
	}

}
