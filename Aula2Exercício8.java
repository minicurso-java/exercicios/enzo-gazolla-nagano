import java.util.Locale;
import java.util.Scanner;

public class Aula2Exercício8 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		
		double x, y;
		
		Scanner out = new Scanner(System.in); 
		
		System.out.println("Digite o valor de X: ");
		x = out.nextDouble();
		System.out.println("Digite o valor de Y: ");
		y = out.nextDouble();
		
		if (x > 0 && y > 0) {
			System.out.println("O ponto está no primeiro quadrante");
		} 
		else if (x < 0 && y > 0) {
			System.out.println("O ponto está no segundo quadrante");
		} 
		else if (x < 0 && y < 0) {
			System.out.println("O ponto está no terceiro quadrante");
		} 
		else if (x > 0 && y < 0){
			System.out.println("O ponto está no quarto quadrante");
		} 
		else if (x == 0 && y == 0) {
			System.out.println("O ponto está na origem");
		} 
		else if (x == 0 && y != 0) {
			System.out.println("O ponto está no eixo y");
		} 
		else if (x != 0 && y == 0) {
			System.out.println("O ponto está no eixo x");
		}

		out.close();
		
	}

}