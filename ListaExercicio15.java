import java.util.Scanner;

public class ListaExercicio15 {
	
	public static boolean ehPerfeito(int num) {
		int soma = 0;
		for (int i = 1; i <= num / 2; i++) {
			if (num % i == 0) {
				soma += i;
			}
		}
		return (soma == num);
	}
	
	
	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		int num;
		
		System.out.println("Insira um numero: ");
		num = out.nextInt();
		
		if (ehPerfeito(num)) {
			System.out.println("É um numero perfeito");		
		} else {
			System.out.println("Nao é um numero perfeito");
		}
		
		out.close();
		
	}

}
