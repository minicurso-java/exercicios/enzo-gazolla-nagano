import java.util.Scanner;

public class Aula2Exercício7 {

	public static void main(String[] args) {
		
		double operando1, operando2;
		int codigo;
		
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite o operando 1:");
		operando1 = out.nextDouble();
		System.out.println("Digite o operando 2:");
		operando2 = out.nextDouble();
		System.out.println("Digite o código (1 - Soma, 2 - Subtração, 3 - Divisão ou 4 - Multiplicação) da operação:");
		codigo = out.nextInt();
		
		if (codigo == 1) {
			System.out.printf("Soma", operando1 + operando2);
		}
		else if (codigo == 2) {
			System.out.printf("Subtração", operando1 - operando2);
		}
		else if (codigo == 3) {
			System.out.printf("Divisão", operando1 / operando2);
		} 
		else if (codigo == 4) {
			System.out.printf("Multiplicação", operando1 * operando2);
		}
		else {
			System.out.println("Operação inválida");
		}
		out.close();
	}

}