import java.util.Scanner;

public class Aula6Exercicio5 {

	public static void main(String[] args) {

		Scanner out = new Scanner (System.in);

		System.out.print("Digite a quantidade de pessoas: ");
		int pessoas = out.nextInt();

		String[] nomes = new String[pessoas];
		int[] idades = new int[pessoas];

		for (int i = 0; i < pessoas; i++) {

			System.out.printf("Digite o nome da pessoa: ");
			nomes[i] = out.next();
			System.out.print("Digite a idade da pessoa: ");
			idades[i] = out.nextInt();
		}

		int pessoaMaisVelha = 0;
		for (int i = 1; i < pessoas; i++) {
			if (idades[i] > idades[pessoaMaisVelha]) {
				pessoaMaisVelha = i;
			}
		}

		System.out.printf("A pessoa mais velha é %s", nomes[pessoaMaisVelha]);

		out.close();
	}

}
