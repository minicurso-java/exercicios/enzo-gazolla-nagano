import java.util.Locale;
import java.util.Scanner;

public class ListaExercicio2 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite o primeiro valor:");
		int valor1 = out.nextInt();
		System.out.println("Digite o segundo valor: ");
		int valor2 = out.nextInt();
		System.out.println("Digite o Terceiro valor: ");
		int valor3 = out.nextInt();
		
		int media = (valor1 + valor2 + valor3)/ 3;
		
		System.out.printf("A média dos valores é %d", media);
		
		out.close();
	}

}
