import java.util.Scanner;

public class ListaExercicio19 {

	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite o raio da esfera: ");
		double raio = out.nextDouble();
		
		double volume = (4 * 3.141516 * Math.pow(raio, 3)) / 3;
		
		System.out.printf("O volume da esfera é: %.2f", volume);
		out.close();
		
	}

}
