import java.util.Scanner;

public class ListaExercicio7 {
	
	public static boolean ehPalindromo(String palavra) {

		char[] charPalavra = palavra.toCharArray();
		int pt1 = 0;
		int pt2 = charPalavra.length - 1;
		while (pt1 != pt2) {
			if (charPalavra[pt1] != charPalavra[pt2]) {
				return false;
			}
			pt1++;
			pt2--;
		}
		return true;
	}

	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);

		System.out.println("Digite uma palavra: ");
		String palavra = out.nextLine();

		char[] charPalavra = palavra.toCharArray();

		for (int i = 0; i < charPalavra.length; i++) {
			if (charPalavra[i] != charPalavra[charPalavra.length - 1 - i]) {
				System.out.println("Não é um palíndromo");
				return;
			}
	
		}
		System.out.println("É um palíndromo");
		out.close();
	}

}
