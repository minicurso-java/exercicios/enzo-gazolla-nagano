
public class Aula8ProdutosEmUmMercado {

	public static void main(String[] args) {
	
	   Produto produto = new Produto("Camiseta", 30.99, 40);
       produto.adicionarAoEstoque(20);
       produto.removerDoEstoque(30);
       
       System.out.printf("Nome: %s, Quantidade: %d, Valor: %.2f \n", produto.nome, produto.quantEmEstoque, produto.preco);
       
       double valorTotalEmEstoque = produto.calcularValorTotalEmEstoque();
       System.out.printf("Valor total em estoque: %.2f ", valorTotalEmEstoque);
   }

}
class Produto {
	
	 String nome;
	 double preco;
	 int quantEmEstoque;
	 
	public Produto(String nome, double preco, int quantEmEstoque) {
		this.nome = nome;
		this.preco = preco;
		this.quantEmEstoque = quantEmEstoque;
		System.out.printf("Nome: %s, Quantidade: %d, Valor: %.2f \n", nome, quantEmEstoque, preco);
	}

	public void adicionarAoEstoque(int quantidade) {
		quantEmEstoque += quantidade;
		System.out.printf("Você adicionou %d no estoque \n", quantidade);
    }
	
	public void removerDoEstoque (int quantidade) {
		
		if (quantidade <= quantEmEstoque) {
			quantEmEstoque -= quantidade;
			System.out.printf("Você removeu %d no estoque \n\n", quantidade);
		} else {
			System.out.println("A quantidade solicitada é maior que a quantidade do estoque");
		}
		
	}
	
	public double calcularValorTotalEmEstoque() {
		return preco * quantEmEstoque;
	}

}