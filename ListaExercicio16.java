import java.util.Scanner;

public class ListaExercicio16 {

	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		String numbinario;
		
		System.out.println("Insira um numero binario");
		numbinario = out.nextLine();
		
		int numdecimal = Integer.parseInt(numbinario, 2);
		
		System.out.printf("O numero decimal é %d ", numdecimal);
		
		out.close();
	}

}
