import java.util.Arrays;
import java.util.Scanner;

public class Aula6Atividade1 {


	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);

		int[] array = new int[7];
		int num;

		System.out.println("Insira um valor: ");
		num = out.nextInt();

		array[0] = num;
		array[6] = num;


		System.out.println(Arrays.toString(array));

		out.close();

	}

}
