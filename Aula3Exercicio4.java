
public class Aula3Exercicio4 {
	
	static int media(int nota1, int nota2, int nota3, char op) {
		if (op == 'a') {
			return mediaAritmetica(nota1, nota2, nota3);
		} else if (op == 'p') {
			return mediaPonderada(nota1, nota2, nota3);
		} else {
			return -1;
		}
	}
	
	static int mediaAritmetica(int nota1, int nota2, int nota3) {
		return (nota1 + nota2 + nota3) / 3;
	}
	
	static int mediaPonderada(int nota1, int nota2, int nota3) {
		int peso1 = 2;
		int peso2 = 3;
		int peso3 = 5;
		return (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);
		}
	
	public static void main(String[] args) {
		int nota1 = 10;
		int nota2 = 9;
		int nota3 = 8;
		char op = 'a';
		
		int media = media(nota1, nota2, nota3, op);
		System.out.println(media);
	}	
	
}