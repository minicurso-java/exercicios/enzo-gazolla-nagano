
public class ListaRevisaoEncapsulamento1 {

	public static void main(String[] args) {

		Usuario usuario = new Usuario(); 
		usuario.setNome("Astrobaldo");
		usuario.setEmail("astrobaldp@gmail.com");
		usuario.setSenha("1234");
		System.out.println(usuario);
	}

}
class Usuario {

	private String nome;
	private String email;
	private String senha;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	@Override
	public String toString() {
		return "Usuario [nome = " + nome + ", email = " + email + ", senha = " + senha + "]";
	}

}
