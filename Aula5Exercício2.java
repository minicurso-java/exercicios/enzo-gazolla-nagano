import java.util.Scanner;

public class Aula5Exercício2 {

	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);

		int alcool = 0;
		int gasolina = 0;
		int diesel = 0;
		int codigo = 0;

		while (codigo != 4) {
			
			System.out.println("Digite um código para continuar(1.alcool, 2. gasolina, 3. diesel e 4. Fim): ");
			codigo = out.nextInt();

			if (codigo == 1) {
				alcool++;
			} else if (codigo == 2) {
				gasolina++;
			} else if (codigo == 3) {
				diesel++;
			} else if (codigo == 4) {
				System.out.println("Fim \n");
			} else {
				System.out.println("Código inválido, coloque um código válido \n");
			}
				
		}
		System.out.println("Saida: ");
		System.out.println("MUITO OBRIGADO");
		System.out.printf("1. alcool = %d\n", alcool);
		System.out.printf("2. gasolina = %d\n", gasolina);
		System.out.printf("3. diesel = %d\n", diesel);

		out.close();

	}

}