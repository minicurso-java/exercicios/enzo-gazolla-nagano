public class ListaRevisaoPolimorfismo2{

	public static void main(String[] args) {
	
		var violao = new Violao();
		System.out.println("Violão: ");
		violao.nota("Dó");
		violao.afinar();

		var piano = new Piano();
		System.out.println("Piano: ");
		piano.nota("Ré");
		piano.afinar();

		var flauta = new Flauta();
		System.out.println("Flauta:");
		flauta.nota("Mi");
		flauta.afinar();

	}

}
interface InstrumentoMusical {
	void nota(String nota);
	void afinar();

}

class Violao implements InstrumentoMusical {
	@Override
	public void nota(String nota) {
		System.out.printf("Violão tocando a nota: %s \n",nota);
	}

	@Override
	public void afinar() {
		System.out.println("Afinando o Violão.");
	}
}

class Piano implements InstrumentoMusical {
	@Override
	public void nota(String nota) {
		System.out.printf("Piano tocando a nota: %s \n", nota);
	}

	@Override
	public void afinar() {
		System.out.println("Afinando o Piano.");
	}
}

class Flauta implements InstrumentoMusical {
	@Override
	public void nota(String nota) {
		System.out.printf("Flauta tocando a nota: %s \n", nota);
	}

	@Override
	public void afinar() {
		System.out.println("Afinando a Flauta.");
	}
}

