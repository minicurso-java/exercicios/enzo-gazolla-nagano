import java.util.Scanner;

public class Aula4Exercicio5 {
    
	public static void main(String[] args) {
        
		Scanner out = new Scanner(System.in);

        System.out.println("Digite um número inteiro positivo N: ");
        int numero = out.nextInt();

        System.out.println("Linha \t | Quadrado \t | Cubo ");

        for (int i = 1; i <= numero; i++) {
            
        	int quadrado = i * i;
            int cubo = i * i * i;
            
            System.out.printf("%d \t | %d \t\t | %d \n", i, quadrado, cubo);
        }

        out.close();
    }
	
}
