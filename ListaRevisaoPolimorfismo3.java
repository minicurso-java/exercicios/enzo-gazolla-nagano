public class ListaRevisaoPolimorfismo3 {

	public static void main(String[] args) {

		Transporte carro = new Carro();
		System.out.println("Carro:");
		carro.acelerar();
		carro.frear();
		carro.virar("direita");

		Transporte moto = new Moto();
		System.out.println("Moto:");
		moto.acelerar();
		moto.frear();
		moto.virar("esquerda");

		Transporte caminhao = new Caminhao();
		System.out.println("Caminhão:");
		caminhao.acelerar();
		caminhao.frear();
		caminhao.virar("direita");
	}

}

interface Transporte {
	void acelerar();
	void frear();
	void virar(String direcao);
}

class Carro implements Transporte {

	@Override
	public void acelerar() {
		System.out.println("Acelerando");
	}

	@Override
	public void frear() {
		System.out.println("Freando");
	}

	@Override
	public void virar(String direcao) {
		System.out.printf("O carro está virando para %s \n\n", direcao);
	}
}

class Moto implements Transporte {

	@Override
	public void acelerar() {
		System.out.println("Acelerando");
	}

	@Override
	public void frear() {
		System.out.println("Freando");
	}

	@Override
	public void virar(String direcao) {
		System.out.printf("A moto está virando para %s \n\n", direcao);
	}
}

class Caminhao implements Transporte {

	@Override
	public void acelerar() {
		System.out.println("Acelerando");
	}

	@Override
	public void frear() {
		System.out.println("Freando");
	}

	@Override
	public void virar(String direcao) {
		System.out.printf("O caminhão está virando para %s \n\n", direcao);
	}
}

