import java.util.Scanner;

public class Aula3ExercicioGarcom2 {

	private static int calculo(Scanner out, int numBandejas, int totalQuebradas) {
		for (int i = 0; i < numBandejas; i++) {

			System.out.println("bandeja " + (i + 1) + ": Digite o número de latas: ");
			int numLatas = out.nextInt();

			System.out.println("bandeja " + (i + 1) +": Digite o número de copos: ");
			int numCopos = out.nextInt();

			if (numLatas > numCopos) {
				totalQuebradas += numCopos;
			}
		}
		return totalQuebradas;
	}

	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);
		System.out.println("Digite o número de bandejas: ");
		int numBandejas = out.nextInt();
		int totalQuebradas = 0;

		totalQuebradas = calculo(out, numBandejas, totalQuebradas);

		if (totalQuebradas == 0) {
			System.out.println("Nenhum copo quebrado");
		} 
		else {
			System.out.println("Total de copos quebrados: " + totalQuebradas);
		}

		out.close();
	}


}
