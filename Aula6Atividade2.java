import java.util.Arrays;
import java.util.Scanner;

public class Aula6Atividade2 {

	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);
		
		int [] array = {1, 2, 3, 4, 5, 6, 7, 8};
		int num, num2;
		
		// Imprimindo usando a função toString
		System.out.println(Arrays.toString(array));
		
		System.out.println("Insira posição 1: ");
		num = out.nextInt();
		
		System.out.println("Insira posição 2: ");
		num2 = out.nextInt();
		
		out.close();
		
		if (num >= array.length || num2 >= array.length) {
			System.out.println("Posições inválidas");
			return;
		}
		
		int temp = array[num];
		array[num] = array[num2];
		array[num2] = temp;
		
		
		// Imprimindo usando for
		System.out.print("[");
		for (int i = 0; i < array.length; i++) {
			 System.out.print(array[i]);
           if (i != array.length - 1) {
        	   System.out.print(",");
           }
            
        }
		System.out.print("]");
	}

}
