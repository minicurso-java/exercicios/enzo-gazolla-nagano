import java.util.Scanner;

public class Aula3Exercicio6 {
	
	public static String removeVogais(String palavra) {
		
		String novaPalavra = "";
		for (int i = 0; i < palavra.length(); i++) {
	        char caractere = palavra.charAt(i);
	        if (caractere!= 'a' && 
	        	caractere != 'e' && 
	        	caractere != 'i' && 
	        	caractere != 'o' && 
	        	caractere != 'u' && 
	        	caractere != 'A' &&
	        	caractere != 'E' &&
	        	caractere != 'I' &&
	        	caractere != 'O' &&
	        	caractere != 'U') {
	            novaPalavra += caractere;
	        }  
	    }
	        return novaPalavra;
	}
	
	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		String p;
		
		System.out.println("Digite uma palavra: ");
		p = out.nextLine();
		
		String resultado = removeVogais(p);
		System.out.println(resultado);
		
		out.close();
	}

}