import java.util.Locale;
import java.util.Scanner;

public class Aula5Exercício5 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner out = new Scanner(System.in);
		
		char entrada = 's';
		double celsius;
		
		
		while (entrada != 'n') {
			
			System.out.println("Digite sua temperatura em celsius: ");
			celsius = out.nextDouble();
			
			double fahrenheit = (celsius* 9.0/5.0) + 32.0;
			System.out.printf("Resultado em Fahrenheit: %.2f \n", fahrenheit);
			
			System.out.println("Deseja repetir (s/n)?");
			entrada = out.next().charAt(0);
		}
		
		out.close();
	}

}