import java.util.Arrays;
import java.util.Scanner;

public class Aula6Atividade3 {

	public static void main(String[] args) {

		Scanner out = new Scanner(System.in);

		System.out.println("Digite o tamanho do array: ");
		int tamanhoArray = out.nextInt();

		out.close();

		int array [] = new int[tamanhoArray];

		int numero = 0;
		int indice = 0;

		while (indice < tamanhoArray) {
			if (ePrimo(numero)) {
				array[indice] = numero;
				indice++;
			}
			numero++;
		}	
		System.out.println(Arrays.toString(array));
	}

	static boolean ePrimo(int num)  {
		if (num == 1) { return false; }
		if (num == 2) { return true; }
		if (num == 3) { return true; }
		if (num % 2 == 0 || num % 3 == 0) {
			return false;
		} else {
			return true;
		}
	}

}
