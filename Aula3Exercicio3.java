
// Tive que pesquisasr na internet, porque ainda não foi dado a aula de arrays.
// O exercício não está pedindo para o usuário declarar as variáveis.

public class Aula3Exercicio3 {

	public static void mediaAritimetica(int ...numero) {

		int total = 0;
		for (int i = 0; i < numero.length; i++) {
			total = total + numero[i];
		}
		System.out.println("Media aritimetica: " + total / numero.length);
		
	}
	
	public static void mediaPonderada (int[] numero, int[] peso) {

		int total = 0;
		int totalPeso = 0;
		for (int i = 0; i < numero.length; i++) {
			total = total + (numero[i] * peso[i]);
			totalPeso = totalPeso + peso[i];
		}
		System.out.println("Media ponderada: " + total / totalPeso);
		
	}
	
	
	public static void main(String[] args) {


		mediaAritimetica(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		mediaPonderada(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
		

	}

}
