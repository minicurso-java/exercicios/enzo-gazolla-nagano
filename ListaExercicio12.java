import java.util.Scanner;

public class ListaExercicio12 {

	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		int [] array = new int [3];
		int [] peso = new int [3];
		
		for (int i = 0; i < 3; i++) {
			System.out.println("Insira um valor: ");
			array[i] = out.nextInt();
			
			System.out.println("Insira o peso do valor: ");
			peso[i] = out.nextInt();
		}
		int total = (array[0] * peso[0] + array[1] * peso[1] + array[2] * peso[2]) / (peso[0] + peso[1] + peso[2]);
		
		System.out.printf("A média ponderada é %d ", total);
		out.close();
		
	}	

}
