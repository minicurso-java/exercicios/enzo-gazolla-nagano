import java.util.Scanner;

public class ListaExercicio11 {

	public static boolean ePrimo(int numero) {
	    int raiz = (int) Math.sqrt(numero);
	    for (int i = 2; i <= raiz; i++) {
	        if (numero % i == 0) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public static void main(String[] args) {
		
		Scanner out = new Scanner(System.in);
		
		System.out.println("Digite um valor: ");
		int valor = out.nextInt();
		
		if (ePrimo(valor)) {
			System.out.println("O valor é primo");
		} else {
			System.out.println("O valor não é primo");
		}
		
		out.close();

	}
}