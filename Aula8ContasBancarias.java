
public class Aula8ContasBancarias {

	public static void main(String[] args) {

		var conta= new ContaBancaria("Enzo", 2305, 385);
		conta.depositar(100);
		conta.sacar(1);
		conta.verficarSaldo();
	}

}
class ContaBancaria {

	String titular;
	int numero;
	double saldo;


	public ContaBancaria(String titular, int numero, double saldo) {
		this.titular = titular;
		this.numero = numero;
		this.saldo = saldo;
		System.out.printf("Criando a conta do %s com saldo %.2f \n", titular, saldo);
	}

	public void depositar (double valor) {
		saldo += valor;
		System.out.printf("Foi depositado %.2f. Saldo atual: %.2f \n", valor, saldo);
	}

	public void sacar (double valor) {
		if (valor > saldo) {
			System.out.println("Você não tem dinheiro mane");
			return;
		}
		saldo -= valor;
		System.out.printf("Foi retirado %.2f. Saldo atual: %.2f \n", valor, saldo);
	}
	
	public void verficarSaldo () {
		System.out.printf("%s conta: %d saldo: %.2f", titular,numero,saldo);
	}


}