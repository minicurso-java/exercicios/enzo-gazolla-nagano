
public class ListaRevisaoHierarquia1 {

	public static void main(String[] args) {

		Smartphone smartphone = new Smartphone("Iphone", 2010);
		smartphone.ligar();
		smartphone.tirarFoto();
		smartphone.fazerChamada(999555);
		smartphone.desligar();

		Tablet tablet = new Tablet("Ipad", 2012);
		tablet.desenhar();
		tablet.assistir();
		tablet.desligar();

		NoteBook notebook  = new NoteBook("Dell", 2015);
		notebook.programar(001);
		notebook.renderizarVideo();
		notebook.desligar();

	}

}

abstract class DispositivoEletronico {

	String marca;
	int ano;
	public DispositivoEletronico(String marca, int ano) {

		this.marca = marca;
		this.ano = ano;
		System.out.printf("Criando: %s %d \n", marca, ano);
	}


}
class Smartphone extends DispositivoEletronico {

	public Smartphone(String marca, int ano) {
		super(marca, ano);	
	}
	void ligar () {
		System.out.println("Trim Trim!");
	}
	void tirarFoto() {
		System.out.println("Click!");
	}
	void fazerChamada(int numero) {
		System.out.printf("Ligando para %d \n", numero);
	}
	void desligar() {
		System.out.println("Tchau! \n");
	}

}

class Tablet extends DispositivoEletronico {

	public Tablet(String marca, int ano) {
		super(marca, ano);

	}
	void desenhar () {
		System.out.println("o  o");
		System.out.println(" v ");
		System.out.println("___");
	}
	void assistir() {
		System.out.printf("Netflix ou HBO \n");
	}
	void desligar() {
		System.out.println("Tchau! \n");
	}

}

class NoteBook extends DispositivoEletronico {

	public NoteBook (String marca, int ano) {
		super(marca, ano);

	}
	void programar (int codigo) {
		System.out.printf("Código: %d \n", codigo);
	}
	void renderizarVideo() {
		System.out.printf("Vídeo \n");
	}
	void desligar() {
		System.out.println("Tchau!");
	}

}
